class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end

def rps_game_winner(game)
  raise WrongNumberOfPlayersError unless game.length == 2

  # The index of the array element of the winner of the game
  beats = {"PP" => 0, "PR" => 0, "PS" => 1, 
           "RR" => 0, "RS" => 0, "RP" => 1,
           "SS" => 0, "SP" => 0, "SR" => 1}
  key = game[0][1] + game[1][1]

  begin
    return game[beats[key]]
  rescue
    
    raise NoSuchStrategyError
  end
end

def rps_tournament_winner(tournament)
  # Innermost game
  if tournament[0][0].instance_of? String
    return rps_game_winner(tournament)
  else
    temp = [rps_tournament_winner(tournament[0]), rps_tournament_winner(tournament[1])]
    return rps_game_winner(temp)
  end
end
