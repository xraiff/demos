def palindrome?(str)
  temp = str.gsub(/\W/,'').downcase
  temp.reverse == temp
end

def count_words(str)
  myhash = {}
  str.downcase.split(/\W/).select {|w| w.length > 0}.each do |word|
    if myhash[word] 
      myhash[word] = myhash[word] + 1
    else
      myhash[word] = 1
    end
  end
  myhash
end
