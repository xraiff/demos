class CartesianProduct
  include Enumerable
  # your code here
    def initialize(a1, a2)
      @a1 = a1
      @a2 = a2
    end

  # implement each
    def each
      @a1.product(@a2)
    end
end
