class Class
  def attr_accessor_with_history(attr_name)
    attr_name = attr_name.to_s
    attr_reader attr_name
    attr_reader attr_name+"_history" 
    class_eval %{def #{attr_name}= (val)
                   if not @#{attr_name}_history
                     @#{attr_name}_history = [nil]
                   end
                   @#{attr_name}_history << val
                   @#{attr_name} = val
                 end}
  end
end

class Foo
  attr_accessor_with_history :bar
end

class Numeric
  @@currencies = {'yen' => 0.013, 'euro' => 1.292, 'rupee' => 0.019, 'dollar' => 1}
  def method_missing(method_id)
    singular_currency = method_id.to_s.gsub( /s$/, '')
    if @@currencies.has_key?(singular_currency)
      self * @@currencies[singular_currency]
    else
      super
    end
  end

  def in(currency)
    singular_currency = currency.to_s.gsub( /s$/, '')
    if @@currencies.has_key?(singular_currency)
      self / @@currencies[singular_currency]
    else
      super
    end
  end
end

class String
  def method_missing(method_id)
    if method_id == :palindrome?
      temp = self.gsub(/\W/,'').downcase
      temp.reverse == temp
    else
      super
    end
  end
end

module Enumerable
#  def method_missing(method_id)
#    if method_id == :palindrome?
#      # compare front to end, iterating towards the middle
#      true
#    else if method_id == :reverse
#      self.each |elem| do
#      
#      end
#    else 
#      super
#    end
#  end

  def palindrome?
    self == self.reverse
  end
end

