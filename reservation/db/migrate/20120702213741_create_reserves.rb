class CreateReserves < ActiveRecord::Migration
  def change
    create_table :reserves do |t|
      t.string :name
      t.datetime :reserve_at
      t.integer :table_num
      t.integer :num_in_party
      t.timestamps
    end
  end
end
