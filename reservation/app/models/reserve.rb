class Reserve < ActiveRecord::Base
  # attr_accessible :title, :body
  validate :valid_reserve_time

  def valid_reserve_time
    if reserve_at.hour > 22 || reserve_at.hour < 17
      self.errors.add :reserve_at, "Invalid time"
    end
  end 
end
